#include "mangaLineSeparator.h"


/*===================================================================
 *
 * main function
 *
 *===================================================================*/
const  double ito::MangaLineSeparate::log_max_ratio(0.1);
const int ito::MangaLineSeparate::removal_laplacian_window(3);
const int ito::MangaLineSeparate::etf_sobel_window(7);
const int ito::MangaLineSeparate::etf_window(5);
const int ito::MangaLineSeparate::aniso_align_window(9);
const double ito::MangaLineSeparate::flog_thresh(0.05);


void ito::MangaLineSeparate::exec(const cv::Mat& src, cv::Mat& dst,
                                  double alpha, double beta) {
    cv::Mat src_img;
    if(src.channels() == 3)cv::cvtColor(src, src_img, CV_BGR2GRAY);
    else if(src.channels() == 4)cv::cvtColor(src, src_img, CV_BGRA2GRAY);
    else src_img = src.clone();

    cv::Mat removal_mask;
    cv::Mat guide;
    cv::Mat ep_mask;
    cv::Mat temp_dst;

    BinarizeUtil bin_util;
    bin_util.make_binary(src_img);


    ito::LoG_component log_obj(alpha, beta,
                               removal_laplacian_window,
                               log_max_ratio);

    log_obj.mk_mask(src_img, temp_dst);
    bin_util.apply_binary_mask(temp_dst,removal_mask);


    ETF_component etf_obj;
    etf_obj.clac_ETF(src_img, guide,
                     etf_sobel_window,
                     etf_window);

    Aniso_component ep_obj;
    ep_obj.exec(src_img, guide, ep_mask,
                aniso_align_window,
                std::max(5, log_obj.get_base_param()-2),
                flog_thresh);

    bin_util.apply_binary_mask(ep_mask,ep_mask);


    Mask_composer composer;
    composer.exec(removal_mask, ep_mask,dst);

}



/*===================================================================
 *
 * make binary image
 *
 *===================================================================*/
ito::BinarizeUtil::BinarizeUtil() :
    k(0.05),
    R(128),
    window(13),
    global_thresh(50)
{

}

ito::BinarizeUtil::~BinarizeUtil()
{

}


void ito::BinarizeUtil::make_binary(const cv::Mat &src){
    cv::Mat temp_src;
    bin_img.create(src.rows,src.cols,CV_8UC1);

    cv::Mat expanded;
    if(src.channels() != 1){
        cv::cvtColor(src,temp_src,CV_BGR2GRAY);
    }
    else{
        temp_src = src.clone();
    }

    int borderSize = window / 2 + 1;
    int kernelPixels = window * window;
    cv::copyMakeBorder(temp_src, expanded, borderSize, borderSize,
                       borderSize, borderSize, cv::BORDER_REPLICATE);

    cv::Mat sum, sqSum;
    cv::integral(expanded, sum, sqSum, CV_64F);

    for(int y = 0; y < src.rows; ++y)
    {
        for(int x = 0; x < src.cols; ++x)
        {
            int kx = x + window;
            int ky = y + window;
            double sumVal = sum.at<double>(ky, kx)
                    - sum.at<double>(ky, x)
                    - sum.at<double>(y, kx)
                    + sum.at<double>(y, x);
            double sqSumVal = sqSum.at<double>(ky, kx)
                    - sqSum.at<double>(ky, x)
                    - sqSum.at<double>(y, kx)
                    + sqSum.at<double>(y, x);

            double mean = sumVal / kernelPixels;
            double var = (sqSumVal / kernelPixels) - (mean * mean);
            if (var < 0.0)
                var = 0.0;
            double stddev = sqrt(var);
            double threshold = mean * (1 + k * (stddev / R - 1));


            if(temp_src.at<uchar>(y,x) <= global_thresh){
                bin_img.at<uchar>(y,x) = 0;
            }
            else{
                if (temp_src.at<uchar>(y, x) < threshold){
                    bin_img.at<uchar>(y,x) = 0;
                }
                else{
                    bin_img.at<uchar>(y,x) = 1;
                }
            }
        }
    }
}



void ito::BinarizeUtil::apply_binary_mask(
        const cv::Mat &src,
        cv::Mat &dst){
    if(src.rows != bin_img.rows || src.cols != bin_img.cols){
        std::cerr << "Sizes of input and ouput should be same.\n";
        std::exit(0);
    }

    cv::Mat mask_sub;
    bin_img.convertTo(mask_sub,CV_32F,1,-1);
    mask_sub = cv::abs(mask_sub);
    mask_sub.convertTo(mask_sub,CV_8U);
    cv::Mat img_255 = cv::Mat::ones(src.size(),CV_8U);
    dst = img_255 - src;
    cv::multiply(dst,mask_sub,dst);

    dst = img_255 - dst;
}



cv::Mat ito::BinarizeUtil::get_binary() const{
    if(bin_img.empty()){
        return cv::Mat();
    }
    else{
        cv::Mat bin_clone = bin_img.clone();
        return bin_clone;
    }
}





/*===================================================================
 *
 * make Removal mask
 *
 *===================================================================*/
ito::LoG_component::LoG_component(double _alpha, double _beta,
                                  int _laplacian_window, double log_max_ratio)
    : stop_limit(_alpha),
      beta(_beta),
      lap_window(_laplacian_window),
      max_ratio(log_max_ratio)
{
}


void ito::LoG_component::mk_mask(const cv::Mat &src, cv::Mat &dst) {
    cv::Mat mask,base_mask;
    int pre_count,post_count;
    double max_window = std::max(src.rows, src.cols) * max_ratio;
    double max_CCC = 0;
    int max_param = 1,param;

    cv::Laplacian(src, src_lap_img, CV_32F, lap_window);

    LoG(src_lap_img, mask,1);
    post_count = Connection::count_4(mask);

    double first_connect = post_count;
    pre_count = post_count * 2;

    for(param = 3;
        std::abs((pre_count - post_count)*post_count/first_connect) >= stop_limit &&
        param < max_window;
        param += 2)
    {
        LoG(src_lap_img, mask, param);
        pre_count = post_count;
        post_count = Connection::count_4(mask);

        double CCC = (double)(pre_count - post_count);
        CCC /= pre_count;

        if(CCC > max_CCC*beta){
            max_CCC = CCC > max_CCC ? CCC : max_CCC;
            max_param = param;
        }
    }

    max_param += 4;

    LoG(src, dst, max_param);

    base_param = (int)(param / 2.0);
    if(base_param % 2 == 0)++base_param;
    base_param = std::min(max_param, base_param);
    LoG(src, base_mask, base_param);

    cv::bitwise_or(dst, base_mask, dst);
}


void ito::LoG_component::LoG(const cv::Mat &src, cv::Mat& dst, int param) {
    if(param != 1){
        cv::Mat kernel = mk_gaussian_kernel(param);
        cv::filter2D(src_lap_img, dst, CV_32F, kernel);
    }
    else{
        dst = src.clone();
    }

    double min,max;
    cv::minMaxIdx(dst, &min, &max);
    dst.convertTo(dst, CV_8U, 255.0/max, 0);
    cv::threshold(dst, dst,0,1,cv::THRESH_BINARY_INV | cv::THRESH_OTSU);
}


cv::Mat ito::LoG_component::mk_gaussian_kernel(int param) {
    int hks = (param - 1) / 2;
    double sigma = param / 6.0;
    double sum = 0;
    cv::Mat kernel(param, param, CV_32F);

    float* row_ptr;
    for(int y = -hks; y <= hks; ++y)
    {
        row_ptr = kernel.ptr<float>(y + hks);
        for (int x = -hks; x <= hks; ++x)
        {
            row_ptr[hks + x] = (float)exp(-0.5*(pow(y, 2) + pow(x, 2)) / pow(sigma, 2));
            sum += row_ptr[hks + x];
        }
    }

    kernel /= sum;
    return kernel;
}






/*===================================================================
 *
 * calc ETF
 *
 *===================================================================*/
void ito::ETF_component::clac_ETF(const cv::Mat &src, cv::Mat &dst,
                                  int sobel_window, int window_size) {
    src_img = src.clone();
    this->window_size = window_size;
    init(sobel_window);

    update_ETF();

    dst = cv::Mat::zeros(src_img.size(), CV_32F);
    int margin = window_size / 2;
    float *x_row_ptr, *y_row_ptr, *dst_ptr;
    for(int i = 0; i < src_img.rows; ++i){
        x_row_ptr = x_vector_img.ptr<float>(i + margin);
        y_row_ptr = y_vector_img.ptr<float>(i + margin);
        dst_ptr = dst.ptr<float>(i);
        for(int j = 0; j < src_img.cols; ++j){
            float dy = y_row_ptr[j + margin];
            float dx = x_row_ptr[j + margin];
            float value;
            if(dy == 0 && dx == 0){
                value = -1;
            }
            else value = cv::fastAtan2(dy, dx);
            dst_ptr[j] = value;
        }
    }

}


void ito::ETF_component::init(int sobel_window) {
    cv::Mat temp_x, temp_y;
    cv::Sobel(src_img, temp_x, CV_32F, 1, 0, sobel_window);
    cv::Sobel(src_img, temp_y, CV_32F, 0, 1, sobel_window);

    int margin = window_size - 1;

    x_vector_img = cv::Mat::zeros(src_img.rows + margin, src_img.cols + margin, CV_32F);
    y_vector_img = cv::Mat::zeros(src_img.rows + margin, src_img.cols + margin, CV_32F);

    temp_x.copyTo(x_vector_img(cv::Rect((int)(window_size*0.5),
                                        (int)(window_size*0.5),
                                        src_img.cols,src_img.rows)));

    temp_y.copyTo(y_vector_img(cv::Rect((int)(window_size*0.5),
                                        (int)(window_size*0.5),
                                        src_img.cols,src_img.rows)));

    normalize();

    cv::magnitude( x_vector_img, y_vector_img, origin_power_img);
}



void ito::ETF_component::update_ETF() {
    cv::Mat pre_x_img = x_vector_img.clone();
    cv::Mat pre_y_img = y_vector_img.clone();

    int margin = (int)(window_size * 0.5);

#pragma omp parallel for
    for(int i = 0; i < src_img.rows; ++i){
        cv::Mat x_sub, y_sub, sub;
        double new_x, new_y;
        float *x_row_ptr = x_vector_img.ptr<float>(i + margin);
        float *y_row_ptr = y_vector_img.ptr<float>(i + margin);
        for(int j = 0; j < src_img.cols; ++j){
            x_sub = pre_x_img(cv::Rect(j, i, window_size, window_size));
            y_sub = pre_y_img(cv::Rect(j, i, window_size, window_size));
            sub = origin_power_img(cv::Rect(j, i, window_size, window_size));
            calc_new_vector(x_sub, y_sub, sub, new_x, new_y);
            x_row_ptr[j + margin] = (float)new_x;
            y_row_ptr[j + margin] = (float)new_y;
        }
    }

    normalize();
}



void ito::ETF_component::calc_new_vector(cv::Mat &x_rect, cv::Mat &y_rect,
                                         cv::Mat &origin_power_rect,
                                         double &x, double &y)
{
    x = y = 0;
    double center_x = x_rect.at<float>(x_rect.rows/2, x_rect.cols/2);
    double center_y = y_rect.at<float>(x_rect.rows/2, x_rect.cols/2);
    double center_power = origin_power_rect.at<float>(x_rect.rows/2, x_rect.cols/2);
    double w_d, w_m;
    double target_x, target_y, target_power;
    if(center_x != 0 || center_y != 0){

        float *x_row_ptr, *y_row_ptr, *origin_power_ptr;

        for(int i = 0; i < x_rect.rows; ++i){
            x_row_ptr = x_rect.ptr<float>(i);
            y_row_ptr = y_rect.ptr<float>(i);
            origin_power_ptr = origin_power_rect.ptr<float>(i);
            for(int j = 0; j  < x_rect.cols; ++j){
                target_x = x_row_ptr[j];
                target_y = y_row_ptr[j];
                target_power = origin_power_ptr[j];
                w_m = 0.5 * (1.0 + tanh(target_power - center_power));
                w_d = center_x*target_x + center_y*target_y;
                x += w_d * w_m * target_x;
                y += w_d * w_m * target_y;
            }
        }
    }
    x /= window_size * window_size;
    y /= window_size * window_size;
}




void ito::ETF_component::normalize() {
    int margin = window_size / 2;
    float *x_row_ptr, *y_row_ptr;
    for(int i = margin; i < src_img.rows + margin; ++i){
        x_row_ptr = x_vector_img.ptr<float>(i);
        y_row_ptr = y_vector_img.ptr<float>(i);
        for(int j = margin; j  < src_img.cols + margin; ++j){
            double x_val = x_row_ptr[j];
            double y_val = y_row_ptr[j];
            double norm = sqrt(x_val*x_val + y_val*y_val);
            if(norm != 0){
                x_row_ptr[j] = (float)(x_val / norm);
                y_row_ptr[j] = (float)(y_val / norm);
            }
        }
    }

}





/*===================================================================
 *
 * make Edge Preserving mask
 *
 *===================================================================*/
void ito::Aniso_component::exec(const cv::Mat& src, cv::Mat _guide, cv::Mat &dst,
                                int _window_m, int _window_s, double threshold){
    origin_row = src.rows;
    origin_col = src.cols;
    window_m = _window_m;
    window_s = _window_s;

    sigma_m = window_m / 3.0;
    sigma_s = window_s / 3.0;
    sigma_c = sigma_s / 1.6;

    extend = window_m > window_s ? window_m : window_s;
    margin = extend / 2;
    this->guide = cv::Mat::zeros(_guide.rows + extend-1, _guide.cols + extend-1, CV_8U);

    float *_guide_ptr;
    unsigned char *guide_ptr;
    for(int i = 0; i < _guide.rows; ++i){
        _guide_ptr = _guide.ptr<float>(i);
        guide_ptr = this->guide.ptr<unsigned char>(i + margin);
        for(int j = 0; j < _guide.cols; ++j){
            double value = _guide_ptr[j];
            if(value != -1){
                value = (int)(((int)(value + 22.5) % 360) / 45);
                value = ((int)value % 4) + 1;
                guide_ptr[j + margin] = (int)value;
            }
        }
    }

    this->src_img = cv::Mat::zeros(src.rows + extend - 1, src.cols + extend - 1, CV_8U);
    src.copyTo(src_img(cv::Rect(margin, margin, src.cols, src.rows)));


    this->mk_gauss();
    this->calc_F();
    this->calc_H();

    cv::Mat sub_H = H_img(cv::Rect(margin, margin, origin_col, origin_row));
    thresh(sub_H, dst, threshold);

}

void ito::Aniso_component::thresh(cv::Mat& img, cv::Mat& dst, double thresh) {
    dst = cv::Mat::zeros(img.size(), CV_8U);

    float *src_ptr;
    unsigned char *dst_ptr;
    for(int i = 0; i < img.rows; ++i){
        src_ptr = img.ptr<float>(i);
        dst_ptr = dst.ptr<unsigned char>(i);
        for(int j = 0; j < img.cols; ++j){
            float value = src_ptr[j];
            if(value < 0 && (1.0 + tanh(value)) < thresh)continue;
            else dst_ptr[j] = 1;
        }
    }
}


void ito::Aniso_component::mk_gauss() {
    const double PI = sqrt(2 * 3.14);
    const double eff_s = 1.0 / (PI * sigma_s);
    const double eff_c = 1.0 / (PI * sigma_c);
    const double eff_m = 1.0 / (PI * sigma_m);
    double sum_s = 0, sum_c = 0, sum_m = 0;
    std::vector<double> gauss_s, gauss_c;
    for(int i = 0; i <= window_s/2; ++i){
        double v_s = eff_s*exp(-(i*i/(2*sigma_s*sigma_s)));
        double v_c = eff_c*exp(-(i*i/(2*sigma_c*sigma_c)));
        sum_s += v_s;
        sum_c += v_c;
        if(i != 0){
            sum_s += v_s;
            sum_c += v_c;
        }
        gauss_s.push_back(v_s);
        gauss_c.push_back(v_c);
    }
    for(int i = 0; i <= window_s/2 ;++i){
        gauss_f.push_back((gauss_c[i] / sum_c) - (gauss_s[i] / sum_s));
    }
    for(int i = 0; i <= window_m/2; ++i){
        double v_m = eff_m * exp(-(i * i / (2 * sigma_m * sigma_m)));
        sum_m += v_m;
        if(i != 0)sum_m += v_m;
        gauss_m.push_back(v_m);
    }
    for(int i = 0; i <= window_m/2; ++i){
        gauss_m[i] = gauss_m[i] / sum_m;
    }
}


void ito::Aniso_component::update_point(int direction, int& prev, int &x, int &y) {
    int sub_prev = 1;
    switch(direction){
    case 0:
        update_point(prev, sub_prev, x, y);
        break;
    case 1:
        --y;
        prev = direction;
        break;
    case 2:
        --x;
        ++y;
        prev = direction;
        break;
    case 3:
        --x;
        prev = direction;
        break;
    case 4:
        --x;
        --y;
        prev = direction;
        break;
    }
}


void ito::Aniso_component::update_inv_point(int direction, int& prev, int &x, int &y) {
    int sub_prev;
    switch(direction){
    case 0:
        update_inv_point(prev, sub_prev, x, y);
        break;
    case 1:
        ++y;
        prev = direction;
        break;
    case 2:
        ++x;
        --y;
        prev = direction;
        break;
    case 3:
        ++x;prev = direction;
        break;
    case 4:
        ++x;
        ++y;
        prev = direction;
        break;
    }
}



void ito::Aniso_component::calc_F() {
    cv::Mat guide_rect;
    cv::Mat value_rect;

    F_img = cv::Mat::zeros(src_img.rows, src_img.cols, CV_32F);
    int start = (extend - window_s) / 2;

    float *F_ptr;
    for(int i = 0; i < origin_row; ++i){
        F_ptr = F_img.ptr<float>(i + margin);
        for(int j = 0; j < origin_col; ++j){
            guide_rect = guide(cv::Rect(start + j, start + i, window_s, window_s));
            value_rect = src_img(cv::Rect(start + j, start + i, window_s, window_s));
            double value = calc_sub_F(value_rect, guide_rect);
            F_ptr[j + margin] = (float)value;
        }
    }
}


double ito::Aniso_component::calc_sub_F(const cv::Mat &rect, const cv::Mat &guide_rect){
    int now_y_in,now_y;
    now_y_in = now_y = rect.rows / 2;
    int now_x_in, now_x;
    now_x_in = now_x = rect.rows / 2;

    size_t rect_elem_step = rect.step / sizeof(unsigned char);
    size_t guide_elem_step = guide_rect.step / sizeof(unsigned char);
    unsigned char *rect_ptr = (unsigned char*)rect.data;
    unsigned char *guide_ptr = (unsigned char*)guide_rect.data;

    double ret = gauss_f[0] * rect_ptr[now_y * rect_elem_step + now_x];
    int prev = 1;
    int direction = guide_ptr[now_y * guide_elem_step + now_x];
    if(direction != 0){
        direction = (direction + 2) % 4;
        if(direction == 0)direction = 4;
    }

    for(int i = 1; i <= window_s/2; ++i){
        this->update_point(direction, prev, now_x, now_y);
        this->update_inv_point(direction, prev, now_x_in, now_y_in);
        ret += gauss_f[i] *
                (rect_ptr[now_y * rect_elem_step + now_x] +
                rect_ptr[now_y_in * rect_elem_step + now_x_in]);
    }

    return ret;
}



void ito::Aniso_component::calc_H() {
    cv::Mat guide_rect;
    cv::Mat value_rect;

    H_img = cv::Mat::zeros(src_img.rows, src_img.cols, CV_32F);
    int start = (extend - window_m) / 2;

    float *H_ptr;
    for(int i = 0; i < origin_row; ++i){
        H_ptr = H_img.ptr<float>(i + margin);
        for(int j = 0; j < origin_col; ++j){
            guide_rect = guide(cv::Rect(start + j, start + i, window_m, window_m));
            value_rect = F_img(cv::Rect_<float>(start + j, start + i, window_m, window_m));
            double value = calc_sub_H(value_rect, guide_rect);
            H_ptr[j + margin] = (float)value;
        }
    }
}


double ito::Aniso_component::calc_sub_H(const cv::Mat &rect, const cv::Mat &guide_rect) {
    int now_y_in, now_y;
    now_y_in = now_y = rect.rows / 2;
    int now_x_in, now_x;
    now_x_in = now_x = rect.rows / 2;

    size_t rect_elem_step = rect.step / sizeof(float);
    size_t guide_elem_step = guide_rect.step / sizeof(unsigned char);
    float *rect_ptr = (float*)rect.data;
    unsigned char *guide_ptr = (unsigned char*)guide_rect.data;

    double ret = gauss_m[0] * rect_ptr[now_y * rect_elem_step + now_x];
    int prev = 1, prev_in = 1;
    int direction = guide_ptr[now_y * guide_elem_step + now_x];
    int direction_in = direction;

    for(int i = 1; i <= window_m/2; ++i){
        update_point(direction, prev, now_x, now_y);
        update_inv_point(direction_in, prev_in, now_x_in, now_y_in);
        ret += gauss_m[i]*
                (rect_ptr[now_y*rect_elem_step + now_x]+
                rect_ptr[now_y_in*rect_elem_step + now_x_in]);
        direction = guide_ptr[now_y*guide_elem_step + now_x];
        direction_in = guide_ptr[now_y_in*guide_elem_step + now_x_in];
    }

    return ret;
}





/*===================================================================
 *
 * conpose Removal mask and Edge Preserving mask
 *
 *===================================================================*/

void ito::Mask_composer::exec(const cv::Mat &del_mask, const cv::Mat &guide_mask,
                              cv::Mat &dst_mask) {
    del = del_mask;
    guide = guide_mask;

    cv::bitwise_and(del,guide, bin_mask);
    dst_mask = cv::Mat::ones(del.size(), CV_8U);

    bin_copy = bin_mask.clone();
    for(int i = 0; i < del.rows; ++i){
        for(int j = 0; j < del.cols; ++j){
            if(del.at<unsigned char>(i, j) == 0){
                update(cv::Point(j, i), dst_mask);
            }
        }
    }
}


void ito::Mask_composer::update(cv::Point start, cv::Mat& dst) {
    cv::Rect rect;
    cv::Mat check_mask;
    cv::floodFill(bin_copy, start, cv::Scalar(1), &rect,
                cv::Scalar(0), cv::Scalar(0), 8);
    cv::bitwise_xor(bin_copy(rect), bin_mask(rect), check_mask);
    cv::bitwise_or(check_mask, del(rect), del(rect));
//    cv::Mat sub;
//    cv::bitwise_xor(check_mask, guide(rect), sub);

//    if(cv::sum(sub)[0] != 0)
        dst(rect) -= check_mask;
}






/*===================================================================
 *
 * count number of connected components
 *
 * may be bottleneck
 *
 *===================================================================*/
int ito::Connection::count_4(cv::Mat& src) {
    int count = 0;
    cv::Rect rect;
    cv::Mat src_copy = src.clone();
    for(int i = 0; i < src.rows; ++i){
        for(int j = 0; j < src.cols; ++j){
            if(src_copy.at<unsigned char>(i, j) == 0){
                cv::floodFill(src_copy, cv::Point(j,i), cv::Scalar(1),
                            &rect, cv::Scalar(0), cv::Scalar(0), 4);
                ++count;
            }
        }
    }
    return count;
}
