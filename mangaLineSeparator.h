#ifndef MANGALINESEPALATOR_H
#define MANGALINESEPALATOR_H

#include <iostream>

#include <opencv2/opencv.hpp>
#include <omp.h>

namespace ito{

class MangaLineSeparate
{
public:
    static void exec(const cv::Mat& src, cv::Mat& dst,
                     double alpha = 1, double beta = 0.8);

private:
    static const double log_max_ratio;
    static const int removal_laplacian_window;

    static const int etf_sobel_window;
    static const int etf_window;

    static const int aniso_align_window;

    static const double flog_thresh;
};



/*===================================================================
 *
 * class : reasonable binary mask maker
 *
 *===================================================================*/
class BinarizeUtil
{
public:
    BinarizeUtil();
    ~BinarizeUtil();

    void make_binary(const cv::Mat& src);
    void apply_binary_mask(const cv::Mat& src, cv::Mat& dst);
    cv::Mat get_binary()const;

private:
    cv::Mat bin_img;
    const double k;
    const double R;
    const int window;
    const int global_thresh;

};




/*===================================================================
 *
 * class : make removal mask
 *
 *===================================================================*/
class LoG_component
{
public:
    LoG_component(double _alpha, double _beta,
                  int _laplacian_window, double log_max_ration);
    void mk_mask(const cv::Mat& src, cv::Mat& dst);
    int get_base_param(){return base_param;}

private:
    const double stop_limit;
    const double beta;
    const int lap_window;
    const double max_ratio;

    int base_param;

    cv::Mat src_lap_img;

    void LoG(const cv::Mat& src, cv::Mat& dst, int param);
    cv::Mat mk_gaussian_kernel(int param);
};




/*===================================================================
 *
 * class : calcurate edge tangent flow
 *
 *===================================================================*/
class ETF_component
{
public:
    void clac_ETF(const cv::Mat& src, cv::Mat& dst,
                  int sobel_window, int window_size);

private:
    cv::Mat src_img;
    cv::Mat origin_power_img;
    cv::Mat x_vector_img;
    cv::Mat y_vector_img;

    int window_size;

    void update_ETF();
    void init(int sobel_window);

    void calc_new_vector(cv::Mat& x_rect, cv::Mat& y_rect,
                         cv::Mat& origin_power_rect,
                         double &x, double &y);
    void normalize();
};





/*===================================================================
 *
 * class : make edge-preserving mask
 *
 *===================================================================*/
class Aniso_component
{
public:
    void exec(const cv::Mat &src, cv::Mat _guide, cv::Mat& dst,
              int _window_m, int _window_s, double threshold);
private:
    cv::Mat guide;
    cv::Mat src_img;
    cv::Mat F_img;
    cv::Mat H_img;
    int origin_row;
    int origin_col;
    int window_m;
    int window_s;
    int extend;
    int margin;
    double sigma_m;
    double sigma_s;
    double sigma_c;
    std::vector<double> gauss_f;
    std::vector<double> gauss_m;

    void mk_gauss();
    void calc_F();
    double calc_sub_F(const cv::Mat& rect, const cv::Mat& guide_rect);
    void calc_H();
    double calc_sub_H(const cv::Mat& rect, const cv::Mat& guide_rect);
    void thresh(cv::Mat& img, cv::Mat& dst, double thresh);

    void update_point(int direction, int& prev, int& x, int& y);
    void update_inv_point(int direction, int& prev, int& x, int& y);
};




/*===================================================================
 *
 *
 *
 *===================================================================*/
class Connection
{
public:
    static int count_4(cv::Mat& img);
};




/*===================================================================
 *
 * class : compose removal mask and edge-preserving mask
 *
 *===================================================================*/
class Mask_composer
{
public:
    void exec(const cv::Mat& del_mask, const cv::Mat& guide_mask,
              cv::Mat& dst_mask);
private:
    cv::Mat del;
    cv::Mat guide;
    cv::Mat bin_mask;
    cv::Mat bin_copy;

    void update(cv::Point start, cv::Mat& dst);
};

}

#endif // MANGALINESEPALATOR_H
