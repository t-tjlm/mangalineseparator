#include <iostream>

#include "mangaLineSeparator.h"

int main(int argc,char* argv[])
{
    cv::Mat input = cv::imread(argv[1]);
    cv::Mat output;

    if(!input.empty()){
        ito::MangaLineSeparate::exec(input,output);

        cv::imshow("input",input);
        cv::imshow("output",output*255);

        cv::waitKey(0);
    }
    else{
        std::cout << "input image cannot be loaded\n";
    }
}

