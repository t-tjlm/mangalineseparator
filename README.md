# README #

### What is this repository for? ###

This is the implementation of "Separation of Manga Line Drawings and Screentones", Eurographics 2015.

### Usage ###

You need only OpenCV 2.4 to integrate this code into your project.
The "main.cpp" shows an example of use.

```
#!c++
// main.cpp
#include <iostream>

#include "mangaLineSeparator.h"

int main(int argc,char* argv[])
{
    cv::Mat input = cv::imread(argv[1]);
    cv::Mat output;

    if(!input.empty()){
        ito::MangaLineSeparate::exec(input,output);

        cv::imshow("input",input);
        cv::imshow("output",output*255);

        cv::waitKey(0);
    }
    else{
        std::cout << "input image cannot be loaded\n";
    }
}

```

You should call only "exec" function.

```
#!c++
ito::MangaLineSeparate::exec(input,output);

```

This function return the binary mask image.